﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;

namespace Server
{
    internal class NameService : Generated.NameRequestService.NameRequestServiceBase
    {
        public override Task<Nothing> Display(NameRequest request, ServerCallContext context)
        {
            System.Console.WriteLine("Hi, {0}!", request.Name);
            return Task.FromResult(new Nothing() { });
        }
    }
}
