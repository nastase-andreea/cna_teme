﻿using Grpc.Core;
using System;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            Console.WriteLine("Hello! What is your name?");

            do
            {
                var nameGiven = Console.ReadLine();

                var client = new Generated.NameRequestService.NameRequestServiceClient(channel);
                client.Display(new Generated.NameRequest
                {
                    Name = nameGiven
                });

            } while (true);


            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
