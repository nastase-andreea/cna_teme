﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Client
{
    class Program
    {
        static bool LeapYear(int year)
        {
            if (year % 4 != 0)
                return false;
            else if (year % 100 != 0)
                return true;
            else if (year % 400 != 0)
                return false;
            else
                return true;
        }
        static bool Validate(string s)
        {
            var regex = @"(0?[1-9]|1[0-2])\/(0?[1-9]|[12]\d|3[10])\/([1-9]\d{0,3})";
            if (s != string.Empty)
            {
                Match match = Regex.Match(s, regex);
                if (match.Value == s)
                {
                    string[] split_s = s.Split('/');
                    int month = int.Parse(split_s[0]);
                    int day = int.Parse(split_s[1]);
                    int year = int.Parse(split_s[2]);

                    if(month == 2)
                    {
                        if (LeapYear(year) == true)
                        {
                            if (day > 29)
                                return false;
                        }
                        else
                            if (day > 28)
                                return false;
                    }
                    else
                    {
                        if ((month == 4 || month == 6) || (month == 9 || month == 11))
                            if (day > 30)
                                return false; 
                    }

                    return true;
                }
                   
            }

            return false;
        }
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            Console.WriteLine("Hello! When is your birthday?");

            var date = Console.ReadLine();
            if (Validate(date))
            {
                var client = new Generated.SignService.SignServiceClient(channel);
                var output = client.Display(new Generated.DateRequest
                {
                    Date = date
                });
                Console.WriteLine("Your sign is {0}.", output.Sign_);
            }
            else
                Console.WriteLine("Your birthday is not a valid date!");

            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
