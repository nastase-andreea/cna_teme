﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class ZodiacSign
    {
        private string name;
        private int beginDay;
        private int beginMonth;
        private int endDay;
        private int endMonth;

        public ZodiacSign(string n = "", int bd = 0, int bm = 0, int ed = 0, int em = 0)
        {
            name = n;
            beginDay = bd;
            beginMonth = bm;
            endDay = ed;
            endMonth = em;
        }

        public string GetName()
        {
            return name;
        }

        public int GetBeginDay()
        {
            return beginDay;
        }
        public int GetBeginMonth()
        {
            return beginMonth;
        }
        public int GetEndDay()
        {
            return endDay;
        }
        public int GetEndMonth()
        {
            return endMonth;
        }
    }

    class SignList
    {
        private List<ZodiacSign> signList = new List<ZodiacSign>();

        public SignList()
        {
            System.IO.StreamReader reader = new System.IO.StreamReader("Zodiac.txt");
            for (string line = reader.ReadLine(); line != null; line = reader.ReadLine())
            {
                string[] sign = line.Split(' ');
                ZodiacSign s = new ZodiacSign(sign[4], int.Parse(sign[0]), int.Parse(sign[1]), int.Parse(sign[2]), int.Parse(sign[3]));
                signList.Add(s);

            }
        }

        public string FindSign(string date)
        {
            string[] dateList = date.Split('/');
            int month = int.Parse(dateList[0]);
            int day = int.Parse(dateList[1]);

            foreach (ZodiacSign sign in signList)
            {
                if ((month == sign.GetBeginMonth() && day >= sign.GetBeginDay())
                    || (month == sign.GetEndMonth() && day <= sign.GetEndDay()))
                    return sign.GetName();
            }

            return string.Empty;
        }
    }
}
