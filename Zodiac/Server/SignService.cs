﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;

namespace Server
{
    internal class SignService : Generated.SignService.SignServiceBase
    {
        public override Task<Sign> Display(DateRequest request, ServerCallContext context)
        {
            SignList signList = new SignList();
            string sign = signList.FindSign(request.Date);
            if ( sign != string.Empty)
                return Task.FromResult(new Sign{ Sign_ = sign });

            return Task.FromResult(new Sign{ Sign_ = string.Empty});
        }
    }
}
